package com.example.i3contacts;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ContactsAdapter extends BaseAdapter {
	ArrayList<ContactRecord> contactsList = null;
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(contactsList == null) return 0;
		
		return contactsList.size();
	}
	public ContactsAdapter(ArrayList<ContactRecord> contactsList) {
		this.contactsList = contactsList;
	}
	
	public void setContactsList(ArrayList<ContactRecord> list) {
		this.contactsList = list;
	}
	
	public void addItem(ContactRecord contact) {
		contactsList.add(contact);
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
			return contactsList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (view == null) {
			LayoutInflater inflater =
			LayoutInflater.from(parent.getContext());
				
			view = inflater.inflate(
					R.layout.contact_list_item, parent, false);
			}
		ContactRecord contact = contactsList.get(index);
		TextView name_view   = (TextView) view.findViewById(R.id.contact_name_view);
		name_view.setText(contact.getName());
		
		TextView number_view = (TextView) view.findViewById(R.id.contact_number_view);
		number_view.setText(contact.getNumber());
		
		return view;
	}
}
