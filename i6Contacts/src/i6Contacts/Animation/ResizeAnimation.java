package i6Contacts.Animation;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {

	private int startHeight;
	private int deltaHeight; // distance between start and end height
	private int start;
	private int end;
	private int delta;
	private int transformationType; // 1 for width, 2 for height, 3 for visible,
									// 4 for invisible
	private View view;

	/**
	 * constructor, do not forget to use the setParams(int, int) method before
	 * starting the animation
	 * 
	 * @param v
	 */
	public ResizeAnimation(View v) {
		this.view = v;
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {

		if (transformationType == 1) {
			view.getLayoutParams().width = (int) (start + delta
					* interpolatedTime);
			view.requestLayout();
		} else if (transformationType == 3) {
			view.setVisibility(View.VISIBLE);
		} else if (transformationType == 4) {
			view.setVisibility(View.INVISIBLE);
		} else if (transformationType == 5) {
			// Show
			((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = (int) (start + delta
					* interpolatedTime);
			view.requestLayout();
		} else if (transformationType == 6) {
			// Hide
			((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = (int) ((start + delta) * interpolatedTime);
			view.requestLayout();
		} else if (transformationType == 7) {
			// Hide
			((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = (int) (start + delta
					* interpolatedTime);
			view.requestLayout();
		} else if (transformationType == 8) {
			// Show
			((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = (int) (start + delta
					* interpolatedTime);
			view.requestLayout();
		} else {
			view.getLayoutParams().height = (int) (startHeight + deltaHeight
					* interpolatedTime);
			view.requestLayout();
		}
	}

	/**
	 * set the starting and ending height for the resize animation starting
	 * height is usually the views current height, the end height is the height
	 * we want to reach after the animation is completed
	 * 
	 * @param start
	 *            height in pixels
	 * @param end
	 *            height in pixels
	 */
	public void setHeightParams(int start, int end) {
		transformationType = 2;
		this.startHeight = start;
		deltaHeight = end - startHeight;
	}

	public void setWidthParams(int start, int end) {
		transformationType = 1;
		this.start = start;
		this.delta = end - start;
	}

	public void setLeftMarginParams(int start, int end) {
		this.setDuration(200);
		if (start == 0)
			transformationType = 6;
		else
			transformationType = 5;
		this.start = start;
		this.delta = end - start;
		this.end = end;
	}

	public void setRightMarginParams(int start, int end) {
		this.setDuration(200);
		if (start == 0)
			transformationType = 7;
		else
			transformationType = 8;
		this.start = start;
		this.delta = end - start;
		this.end = end;
	}

	public void setVisible() {
		transformationType = 3;
	}

	public void setInvisible() {
		transformationType = 4;
	}

	/**
	 * set the duration for the hideshowanimation
	 */
	@Override
	public void setDuration(long durationMillis) {
		super.setDuration(durationMillis);
	}

	@Override
	public boolean willChangeBounds() {
		return true;
	}
}
