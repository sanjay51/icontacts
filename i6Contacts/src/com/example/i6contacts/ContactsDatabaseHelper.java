package com.example.i6contacts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactsDatabaseHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "i5Contacts.db";
	private static final String TABLE_FAV_CONTACTS    = "favContactsRecord";
	
	private static final String FIELD_NAME    = "name";
	private static final String FIELD_NUMBER  = "number";
	private static final String FIELD_EMAIL   = "email";
	
	private OpenHelper openHelper;
	private SQLiteDatabase database;
	
	public ContactsDatabaseHelper(Context context) {
		openHelper = new OpenHelper(context);
		database   = openHelper.getWritableDatabase();
	}
	
	public Cursor getFavContacts() {
		return database.rawQuery("select * from " + TABLE_FAV_CONTACTS, null);
	}
	
	public void addFavContact(String name, String number, String email) {
		ContentValues dbContent = new ContentValues();
		dbContent.put(FIELD_NAME, name);
		dbContent.put(FIELD_NUMBER, number);
		dbContent.put(FIELD_EMAIL, email);
		
		database.insert(TABLE_FAV_CONTACTS, null, dbContent);
	}
	
	public void saveContact(String name, String number, String email) {
		ContentValues dbContent = new ContentValues();
		dbContent.put(FIELD_NAME, name); 
		dbContent.put(FIELD_NUMBER, number);
		dbContent.put(FIELD_EMAIL, email);
		
		database.insert(TABLE_FAV_CONTACTS, null, dbContent);
	}

	private class OpenHelper extends SQLiteOpenHelper {
		public OpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database) {
			// TODO Auto-generated method stub
			database.execSQL(
					"create table " + TABLE_FAV_CONTACTS +
					" ( id integer primary key, name text, number text, email text)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase database, int arg1, int arg2) {
			// TODO Auto-generated method stub
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_FAV_CONTACTS);
			onCreate(database);
		}
	}
}
