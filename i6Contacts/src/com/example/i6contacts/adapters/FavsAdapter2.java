package com.example.i6contacts.adapters;

import java.util.ArrayList;
import java.util.List;

import com.example.i6contacts.R;
import com.example.i6contacts.R.id;
import com.example.i6contacts.R.layout;
import com.example.i6contacts.model.CallRecord;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FavsAdapter2 extends BaseAdapter {
	List<CallRecord> callsList = null;
	List<CallRecord> allCalls = null;

	@Override
	public int getCount() {
		if (callsList == null)
			return 0;

		return callsList.size();
	}

	public FavsAdapter2(List<CallRecord> clist) {
		callsList = new ArrayList<CallRecord>();
		callsList.clear();
		this.allCalls = clist;
		callsList.addAll(allCalls);
		this.notifyDataSetChanged();
	}

	public void setcallsList(ArrayList<CallRecord> list) {
		this.callsList = list;
	}

	public void addItem(CallRecord contact) {
		callsList.add(contact);
	}

	@Override
	public Object getItem(int arg0) {
		return callsList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	String colors[] = {"#0086BF", "#10047A", "#5C0963", "#AD4B3E"};
	static int color = 0;
	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());

			view = inflater.inflate(R.layout.tile_small, parent, false);
		}
		view.findViewById(R.id.tile_view).setBackgroundColor(Color.parseColor(colors[color]));
		color = (color+1)%4;
		CallRecord contact = callsList.get(index);
		TextView name_view = (TextView) view
				.findViewById(R.id.contact_name_view);
		name_view.setText(contact.getName());

		//TextView number_view = (TextView) view
		//		.findViewById(R.id.contact_number_view);
		//number_view.setText(contact.getNumber());

		return view;
	}

	public void filter(String filterStr) {
		int startIndex = 0;
		filterStr = filterStr.toLowerCase();
		callsList.clear();
		if (filterStr.length() == 0) {
			callsList.addAll(allCalls);
		} else {
			for (CallRecord contact : allCalls) {
				if (contact.getName().toLowerCase().startsWith(filterStr)) {
					callsList.add(startIndex, contact);
					startIndex++;
				} else if (contact.getName().toLowerCase().contains(filterStr)) {
					callsList.add(contact);
				}
			}
		}
		notifyDataSetChanged();
	}
}
