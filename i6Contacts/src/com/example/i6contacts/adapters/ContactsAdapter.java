package com.example.i6contacts.adapters;

import java.util.ArrayList;

import com.example.i6contacts.R;
import com.example.i6contacts.R.id;
import com.example.i6contacts.R.layout;
import com.example.i6contacts.model.ContactRecord;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ContactsAdapter extends BaseAdapter {
	static ArrayList<ContactRecord> contactsList = null;
	ArrayList<ContactRecord> allContacts = null;

	@Override
	public int getCount() {
		if (contactsList == null)
			return 0;

		return contactsList.size();
	}

	public ContactsAdapter(ArrayList<ContactRecord> clist) {
		contactsList = new ArrayList<ContactRecord>();
		contactsList.clear();
		this.allContacts = clist;
		contactsList.addAll(allContacts);
		this.notifyDataSetChanged();
	}

	public void setContactsList(ArrayList<ContactRecord> list) {
		this.contactsList = list;
	}

	public void addItem(ContactRecord contact) {
		contactsList.add(contact);
	}

	@Override
	public ContactRecord getItem(int arg0) {
		return contactsList.get(arg0);
	}
	
	public static ContactRecord getItemByIndex(int index) {
		return contactsList.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			view = inflater.inflate(R.layout.contact_list_item, parent, false);
		}
		ContactRecord contact = contactsList.get(index);
		TextView name_view = (TextView) view
				.findViewById(R.id.contact_name_view);
		name_view.setText(contact.getName());

		//TextView number_view = (TextView) view
		//		.findViewById(R.id.contact_number_view);
		//number_view.setText(contact.getNumber());

		return view;
	}

	public void filter(String filterStr) {
		int startIndex = 0;
		filterStr = filterStr.toLowerCase();
		contactsList.clear();
		if (filterStr.length() == 0) {
			contactsList.addAll(allContacts);
		} else {
			for (ContactRecord contact : allContacts) {
				if (contact.getName().toLowerCase().startsWith(filterStr)) {
					contactsList.add(startIndex, contact);
					startIndex++;
				} else if (contact.getName().toLowerCase().contains(filterStr)) {
					contactsList.add(contact);
				}
			}
		}
		notifyDataSetChanged();
	}
}
