package com.example.i6contacts.adapters;

import java.util.ArrayList;

import com.example.i6contacts.R;
import com.example.i6contacts.R.id;
import com.example.i6contacts.R.layout;
import com.example.i6contacts.model.ContactRecord;

import android.graphics.Color;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FavsAdapter extends BaseAdapter {
	ArrayList<ContactRecord> contactsList = null;
	ArrayList<ContactRecord> allContacts = null;

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (contactsList == null)
			return 0;

		return contactsList.size();
	}

	public FavsAdapter(ArrayList<ContactRecord> clist) {
		contactsList = new ArrayList<ContactRecord>();
		contactsList.clear();
		this.allContacts = clist;
		contactsList.addAll(allContacts);
		this.notifyDataSetChanged();
	}

	public void setContactsList(ArrayList<ContactRecord> list) {
		this.contactsList = list;
	}

	public void addItem(ContactRecord contact) {
		contactsList.add(contact);
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return contactsList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	String colors[] = { "#FF0097", "#00ABA9", "#8CBF26", "#A05000", "#E671B8",
			"#F09609", "#1BA1E2", "#E51400", "#339933" };
	// String colors[] = {"#0086BF", "#10047A", "#5C0963", "#AD4B3E"};
	static int color = 6;

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());

			view = inflater.inflate(R.layout.tile_big, parent, false);
		}
		view.findViewById(R.id.tile_view).setBackgroundColor(
				Color.parseColor(colors[color]));
		color = (color + 1) % 9;
		ContactRecord contact = contactsList.get(index);
		TextView name_view = (TextView) view
				.findViewById(R.id.contact_name_view);
		name_view.setText(contact.getName());

		TextView number_view = (TextView) view
				.findViewById(R.id.contact_number_view);
		number_view.setText(contact.getNumber());

		ImageView type_view = (ImageView) view.findViewById(R.id.imageView1);
		int res = R.drawable.mobile;
		switch (contact.getType()) {
		case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
			res = R.drawable.work;
			break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
			res = R.drawable.home;
		}
		type_view.setBackgroundResource(res);

		return view;
	}

	public void filter(String filterStr) {
		int startIndex = 0;
		filterStr = filterStr.toLowerCase();
		contactsList.clear();
		if (filterStr.length() == 0) {
			contactsList.addAll(allContacts);
		} else {
			for (ContactRecord contact : allContacts) {
				if (contact.getName().toLowerCase().startsWith(filterStr)) {
					contactsList.add(startIndex, contact);
					startIndex++;
				} else if (contact.getName().toLowerCase().contains(filterStr)) {
					contactsList.add(contact);
				}
			}
		}
		notifyDataSetChanged();
	}
}
