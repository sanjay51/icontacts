package com.example.i6contacts.adapters;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.example.i6contacts.R;
import com.example.i6contacts.R.id;
import com.example.i6contacts.R.layout;
import com.example.i6contacts.model.CallRecord;
import com.example.i6contacts.model.ContactRecord;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CallsAdapter extends BaseAdapter {
	ArrayList<CallRecord> callsList = null;
	ArrayList<CallRecord> allCalls = null;

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (callsList == null)
			return 0;

		return callsList.size();
	}

	public CallsAdapter(ArrayList<CallRecord> clist) {
		callsList = new ArrayList<CallRecord>();
		callsList.clear();
		this.allCalls = clist;
		callsList.addAll(allCalls);
		this.notifyDataSetChanged();
	}

	public void setContactsList(ArrayList<CallRecord> list) {
		this.callsList = list;
	}

	public void addItem(CallRecord contact) {
		callsList.add(contact);
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return callsList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());

			view = inflater.inflate(R.layout.call_list_item, parent, false);
		}
		CallRecord call = callsList.get(index);
		TextView prime_view = (TextView) view.findViewById(R.id.prime_view3);
		prime_view.setText("" + call.getName());

		TextView number_view = (TextView) view.findViewById(R.id.number_view3);
		number_view.setText("" + call.getNumber());

		TextView duration_view = (TextView) view
				.findViewById(R.id.duration_view3);
		duration_view.setText(getFullTimeFromDuration(call.getDuration()));

		TextView date_view = (TextView) view.findViewById(R.id.time_view3);
		date_view.setText(getFormattedDate(call.getDate()));

		ImageView image_view = (ImageView) view.findViewById(R.id.call_image3);
		int resId = 1;
		switch (call.getType()) {
		case 1:
			resId = R.drawable.ic_call_log_list_incoming_call;
			break;
		case 2:
			resId = R.drawable.ic_call_log_list_outgoing_call;
			break;
		case 3:
			resId = R.drawable.ic_call_log_header_missed_call;
			break;
		default:
			resId = R.drawable.ic_call_log_header_missed_call;
		}
		image_view.setImageResource(resId);

		return view;
	}

	public String getFullTimeFromDuration(int seconds) {
		StringBuilder time = new StringBuilder();
		int hours = seconds / 3600;
		int minutes = (seconds / 60) % 60;
		seconds = seconds % 60;
		if (hours > 0)
			if (hours <= 9)
				time.append("0" + hours + "h ");
			else
				time.append(hours + "h ");
		if (minutes > 0)
			if (minutes <= 9)
				time.append("0" + minutes + "m ");
			else
				time.append(minutes + "m ");

		if (seconds <= 9)
			time.append("0" + seconds + "s");
		else
			time.append(seconds + "s");

		return time.toString();
	}
	
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
	SimpleDateFormat sameDayFmt = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
	SimpleDateFormat allDayFmt = new SimpleDateFormat("dd/MM/yy hh:mm aa", Locale.getDefault());
	
	public String getFormattedDate(long date1) {
		String dateString = "";
		Date thatday = new Date(date1);
		
		Calendar c = Calendar.getInstance(); 
		Date today =  c.getTime();
		
		c.add(Calendar.DATE, -1);
		Date yesterday =  c.getTime();
		
		boolean sameDay = fmt.format(today).equals(fmt.format(thatday));
		if(sameDay == true) {
			dateString = "Today, " + sameDayFmt.format(thatday);
			return dateString;
		}
		
		boolean yesterDay = fmt.format(yesterday).equals(fmt.format(thatday));
		if(yesterDay == true) {
			dateString = "Yesterday, " + sameDayFmt.format(thatday);
			return dateString;
		}
		
		dateString = allDayFmt.format(thatday);
		
	    return dateString;
	}
}
