package com.example.i6contacts;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import com.example.i6contacts.model.CallRecord;
import com.example.i6contacts.model.ContactRecord;
import com.example.i6contacts.utils.FrequencyComparator;
import com.example.i6contacts.utils.IntCallRecordPair;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;

public class ContactsManager {
	public static ArrayList<ContactRecord> ContactsList = null;
	public static ArrayList<ContactRecord> FavsList = null;
	public static ArrayList<CallRecord> CallsList = null;
	public static List<CallRecord> FavCallsList = null;
	HashMap<String, IntCallRecordPair> frequencyMap = null;
	PriorityQueue<IntCallRecordPair> callsPQ = null;

	public static ContactRecord activeContact = null;
	public static CallRecord activeCall = null;

	public static String activeNumber = null;
	public static int isLastActiveCall = 0;

	private static Context mainContext = null;
	// ContactsDatabaseHelper databaseHelper;

	public static ContactsManager contactsManager;

	static ContactsManager getContactsManager(Context context) {
		if (contactsManager == null) {
			contactsManager = new ContactsManager(context);
		}
		return contactsManager;
	}

	private ContactsManager(Context context) {
		mainContext = context;
		ContactsList = new ArrayList<ContactRecord>();
		FavsList = new ArrayList<ContactRecord>();
		CallsList = new ArrayList<CallRecord>();
		FavCallsList = new ArrayList<CallRecord>();
		// databaseHelper = new ContactsDatabaseHelper(mainContext);
		initialize();
	}

	public void initialize() {
		readContacts();
		readCallLog();
		if (isFirstRun()) {
			createFrequencyMap();
			// printFrequencyMap();
			createPQ();
			// printPQ();
			createFavCallList();
		}

	}

	public void readContacts() {
		final String[] projection = new String[] {
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
				ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY,
				ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.TYPE,
				ContactsContract.CommonDataKinds.Phone.STARRED };

		Cursor cursor = mainContext.getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				projection,
				null,
				null,
				ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY
						+ " ASC");
		if (cursor == null)
			return;
		while (cursor.moveToNext()) {
			String contactId = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
			String name = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));
			String number = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			int isStarred = cursor
					.getInt(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
			int type = cursor
					.getInt(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
			ContactRecord record = new ContactRecord(contactId, name, number,
					type);
			ContactsList.add(record);
			if (isStarred == 1)
				FavsList.add(record);
		}
	}

	public static void readCallLog() {
		int favCallCount = 0;
		CallRecord prevRecord = null;
		String[] strFields = { CallLog.Calls._ID, CallLog.Calls.NUMBER,
				CallLog.Calls.TYPE, CallLog.Calls.CACHED_NAME,
				CallLog.Calls.DATE, CallLog.Calls.DURATION };
		String strOrder = CallLog.Calls.DATE + " DESC LIMIT 0,100";

		Cursor cursor = mainContext.getContentResolver().query(
				CallLog.Calls.CONTENT_URI, strFields, null, null, strOrder);

		if (cursor == null)
			return;
		while (cursor.moveToNext()) {
			long date = cursor.getLong(cursor
					.getColumnIndex(CallLog.Calls.DATE));
			String name = cursor.getString(cursor
					.getColumnIndex(CallLog.Calls.CACHED_NAME));
			String number = cursor.getString(cursor
					.getColumnIndex(CallLog.Calls.NUMBER));
			int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
			int duration = cursor.getInt(cursor
					.getColumnIndex(CallLog.Calls.DURATION));
			if (duration == 0 && type == 1)
				continue; // ignore SMSs

			CallRecord record = new CallRecord(date, name, number, type,
					duration);

			CallsList.add(record);
		}
	}

	public static void setActiveContact(ContactRecord c) {
		activeContact = c;
		isLastActiveCall = 0;
		setActiveNumber(c.getNumber());
	}

	public static ContactRecord getActiveContact() {
		return activeContact;
	}

	public static String getActiveNumber() {
		return activeNumber;
	}

	public static void setActiveNumber(String number) {
		activeNumber = number;
	}

	public void callActiveNumber() {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + getActiveNumber()));
		mainContext.startActivity(callIntent);

	}

	public static void setActiveCall(CallRecord call) {
		activeCall = call;
		isLastActiveCall = 1;
		setActiveNumber(call.getNumber());
	}

	public static CallRecord getActiveCall() {
		return activeCall;
	}

	public boolean isFirstRun() {
		return true;
	}

	public void createFrequencyMap() {
		frequencyMap = new HashMap<String, IntCallRecordPair>();
		for (int i = 0; i < CallsList.size(); i++) {
			String name = CallsList.get(i).getName();
			if (frequencyMap.containsKey(name)) {
				frequencyMap.get(name).count += 1;
			} else {
				frequencyMap.put(name, new IntCallRecordPair(CallsList.get(i),
						1));
			}
		}
	}

	public void printFrequencyMap() {
		for (Entry<String, IntCallRecordPair> e : frequencyMap.entrySet()) {
			String key = e.getKey();
			int count = e.getValue().count;
			Log.d("key-value", key + " ->" + count);
		}
	}

	public void printPQ() {
		Iterator<IntCallRecordPair> it = callsPQ.iterator();
		while (it.hasNext()) {
			IntCallRecordPair pair = it.next();
			Log.d("MostFrequentCalls",
					pair.count + " -- " + pair.record.getName());
		}
	}

	public void createPQ() {
		Comparator<IntCallRecordPair> comparator = new FrequencyComparator();
		callsPQ = new PriorityQueue<IntCallRecordPair>(20, comparator);
		for (Entry<String, IntCallRecordPair> e : frequencyMap.entrySet()) {
			IntCallRecordPair pair = e.getValue();
			if (callsPQ.size() < 20) {
				callsPQ.add(pair);
			} else {
				if (callsPQ.peek().count < pair.count) {
					callsPQ.remove();
					callsPQ.add(pair);
				}
			}
		}
	}

	public void createFavCallList() {
		Iterator<IntCallRecordPair> it = callsPQ.iterator();
		while (callsPQ.size() > 0) {
			IntCallRecordPair pair = callsPQ.remove();
			FavCallsList.add(0, pair.record);
		}
		/*
		 * while(it.hasNext()) { IntCallRecordPair pair = it.next();
		 * FavCallsList.add(0, pair.record); }
		 */
	}

	public static int isLastActiveCall() {
		// returns 1 if last event was a call-click in fav list
		return isLastActiveCall;
	}

	public static String getContactIdByName(String name) {
		try {
			Iterator<ContactRecord> it = ContactsList.iterator();
			ContactRecord record = null;
			while (it.hasNext()) {
				record = it.next();
				if (record.getName().compareTo(name) == 0) {
					break;
				}
			}
			if (!it.hasNext())
				return null;

			return record.getContactId();
		} catch (Exception e) {
			return null;
		}
	}
}
