package com.example.i6contacts.model;

public class ContactRecord {
	private String contactId;
	private String name;
	private String number;
	private int type;
	
	public ContactRecord(String contactId, String name, String number, int type) {
		this.setContactId(contactId);
		this.setName(name);
		this.setNumber(number);
		this.setType(type);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	

}
