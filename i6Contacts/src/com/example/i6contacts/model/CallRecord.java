package com.example.i6contacts.model;

public class CallRecord {
	private long date = 0;
	private String name = null;
	private String number = null;
	private int type = 0;
	private int duration = 0;

	public CallRecord(long date, String name, String number, int type,
			int duration) {
		this.setDate(date);
		this.setName(name);
		this.setNumber(number);
		this.setType(type);
		this.setDuration(duration);
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getName() {
		if (name == null) {
			return number;
		}
		return name;
	}

	public boolean isNameAvailble() {
		if (name == null) {
			return false;
		}
		return true;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}