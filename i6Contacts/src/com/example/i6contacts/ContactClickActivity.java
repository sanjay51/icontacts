package com.example.i6contacts;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ContactClickActivity extends Activity {
	private String contact_number;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_click);

		Intent intent = getIntent();
		String contactId = intent.getStringExtra("contactId");
		TextView contactNameView = (TextView) findViewById(R.id.contact_name);
		populateContactDetails(contactId);

		// add PhoneStateListener
		PhoneCallListener phoneListener = new PhoneCallListener();
		TelephonyManager telephonyManager = (TelephonyManager) this
				.getSystemService(TELEPHONY_SERVICE);
		telephonyManager.listen(phoneListener,
				PhoneStateListener.LISTEN_CALL_STATE);
		// String contactDetails = setContactDetails(contactId);
		// contactNameView.setText(contactDetails);
	}

	public void onBtnSmsClick(View view) {
		try {

			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			smsIntent.putExtra("sms_body", "");
			smsIntent.setData(Uri.parse("smsto:"));
			smsIntent.putExtra("address", contact_number);
			smsIntent.setType("vnd.android-dir/mms-sms");
			startActivity(smsIntent);

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"SMS faild :(", Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}
	}

	public void onCallClick(View view) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + contact_number));
		startActivity(callIntent);
	}

	public void populateContactDetails(String contactId) {
		StringBuilder output = new StringBuilder();
		String projection[] = { ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };
		String selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID
				+ "=?";
		String selectionArgs[] = { contactId };

		Cursor cursor = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
				selection, selectionArgs, null);
		if (cursor == null)
			return;
		int firstIter = 1;
		String number = "";
		if (cursor.moveToFirst()) {
			do {
				String type = "--"; // cursor
				// .getString(cursor
				// .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE));
				String name = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				number += cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

				// populate data
				if (firstIter == 1) {
					((TextView) findViewById(R.id.contact_name)).setText(name);
					((TextView) findViewById(R.id.contact_number))
							.setText(number);
					this.contact_number = number;
					firstIter = 0;
					number = "";
				} else {
					((TextView) findViewById(R.id.txtOtherNums))
							.setText(number);
					number += " ,";
				}
			} while (cursor.moveToNext());
		}
	}

	public class PhoneCallListener extends PhoneStateListener {

		private boolean isPhoneCalling = false;

		String LOG_TAG = "LOGGING 123";

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			if (TelephonyManager.CALL_STATE_RINGING == state) {
				// phone ringing
				Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
			}

			if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
				// active
				Log.i(LOG_TAG, "OFFHOOK");

				isPhoneCalling = true;
			}

			if (TelephonyManager.CALL_STATE_IDLE == state) {
				// run when class initial and phone call ended,
				// need detect flag from CALL_STATE_OFFHOOK
				Log.i(LOG_TAG, "IDLE");

				if (isPhoneCalling) {

					Log.i(LOG_TAG, "restart app");

					// restart app
					Intent i = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(
									getBaseContext().getPackageName());
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);

					isPhoneCalling = false;
				}

			}
		}
	}
	/*
	 * public void onCancel(View view) { finish(); } public void onSave(View
	 * view) { Intent intent = getIntent(); EditText nameView = (EditText)
	 * findViewById(R.id.contact_name); intent.putExtra("name",
	 * nameView.getText().toString());
	 * 
	 * EditText numberView = (EditText) findViewById(R.id.contact_number);
	 * intent.putExtra("number", numberView.getText().toString());
	 * 
	 * this.setResult(RESULT_OK, intent); finish(); }
	 */
}
