package com.example.i6contacts.utils;

import java.util.Comparator;

public class FrequencyComparator implements Comparator<IntCallRecordPair> {

	@Override
	public int compare(IntCallRecordPair pair1, IntCallRecordPair pair2) {
		if(pair1.count < pair2.count)
			return -1;
		else if(pair1.count == pair2.count)
			return 0;
		
		return 1;
	}
}
