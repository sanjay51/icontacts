package com.example.i6contacts.utils;

import com.example.i6contacts.model.CallRecord;

public class IntCallRecordPair {
	public int count;
	public CallRecord record;
	
	public IntCallRecordPair(CallRecord r, int c) {
		count = c;
		record = r;
	}
}
