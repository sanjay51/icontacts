package com.example.i6contacts.fragments;

import i6Contacts.Animation.ResizeAnimation;

import com.example.i6contacts.ContactsManager;
import com.example.i6contacts.R;
import com.example.i6contacts.adapters.AppsAdapter;
import com.example.i6contacts.adapters.CallsAdapter;
import com.example.i6contacts.adapters.ContactsAdapter;
import com.example.i6contacts.adapters.FavsAdapter;
import com.example.i6contacts.adapters.FavsAdapter2;
import com.example.i6contacts.model.CallRecord;
import com.example.i6contacts.model.ContactRecord;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class AppsFragment extends Fragment {
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_apps,
				container, false);
		
		GridView gridView = (GridView) rootView.findViewById(R.id.grid_view);

		// Instance of ImageAdapter Class
		gridView.setAdapter(new AppsAdapter(getActivity().getApplicationContext()));
	
		return rootView;
	}
}