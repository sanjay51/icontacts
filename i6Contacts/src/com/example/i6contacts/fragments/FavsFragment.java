package com.example.i6contacts.fragments;

import i6Contacts.Animation.ResizeAnimation;

import com.example.i6contacts.ContactsManager;
import com.example.i6contacts.R;
import com.example.i6contacts.adapters.CallsAdapter;
import com.example.i6contacts.adapters.ContactsAdapter;
import com.example.i6contacts.adapters.FavsAdapter;
import com.example.i6contacts.adapters.FavsAdapter2;
import com.example.i6contacts.model.CallRecord;
import com.example.i6contacts.model.ContactRecord;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FavsFragment extends Fragment {

	public ListView contacts;
	public ListView callsView;
	public FavsAdapter contactsAdapter;
	public FavsAdapter2 callsAdapter;

	public FavsFragment() {
	}

	RelativeLayout optionsBar = null;
	int isOptionsBarOpen = 0;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				ContactRecord contact = (ContactRecord) contactsAdapter
						.getItem(position);
				String contactId = contact.getContactId();
				ContactsManager.setActiveContact(contact);

				if (optionsBar == null) {
					optionsBar = (RelativeLayout) getActivity().findViewById(
							R.id.divFavPanel1);
				}
				((TextView) optionsBar.findViewById(R.id.fav_name1))
						.setText(contact.getName());
				showFavsPanel();
			}
		});
		contacts.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
				hideFavsPanel();
			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				// TODO Auto-generated method stub

			}

		});

		callsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				CallRecord call = (CallRecord) callsAdapter.getItem(position);
				String contactId = null;
				ContactsManager.setActiveCall(call);

				if (optionsBar == null) {
					optionsBar = (RelativeLayout) getActivity().findViewById(
							R.id.divFavPanel1);
				}
				((TextView) optionsBar.findViewById(R.id.fav_name1))
						.setText(call.getName());
				showFavsPanel();
			}
		});
		callsView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
				hideFavsPanel();
			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				// TODO Auto-generated method stub

			}

		});

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_favs_tiles,
				container, false);
		contacts = (ListView) rootView.findViewById(R.id.contacts_list);
		contactsAdapter = new FavsAdapter(ContactsManager.FavsList);
		contacts.setAdapter(contactsAdapter);

		callsView = (ListView) rootView.findViewById(R.id.calls_list);
		callsAdapter = new FavsAdapter2(ContactsManager.FavCallsList);
		callsView.setAdapter(callsAdapter);

		return rootView;
	}

	void hideFavsPanel() {
		if (optionsBar == null) {
			optionsBar = (RelativeLayout) getActivity().findViewById(
					R.id.divFavPanel1);
		}
		if (isOptionsBarOpen == 1) {
			try {
				ResizeAnimation anim = new ResizeAnimation(optionsBar);
				anim.setDuration(500);
				anim.setRightMarginParams(0, -300);
				optionsBar.startAnimation(anim);
				isOptionsBarOpen = 0;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	void showFavsPanel() {
		optionsBar = (RelativeLayout) getActivity().findViewById(
				R.id.divFavPanel1);
		ResizeAnimation anim = new ResizeAnimation(optionsBar);
		anim.setDuration(500);
		anim.setRightMarginParams(-300, 0);
		optionsBar.startAnimation(anim);
		isOptionsBarOpen = 1;
	}
}