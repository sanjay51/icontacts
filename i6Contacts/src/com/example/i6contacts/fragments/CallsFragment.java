package com.example.i6contacts.fragments;

import i6Contacts.Animation.ResizeAnimation;

import com.example.i6contacts.ContactsManager;
import com.example.i6contacts.R;
import com.example.i6contacts.adapters.CallsAdapter;
import com.example.i6contacts.adapters.ContactsAdapter;
import com.example.i6contacts.model.CallRecord;
import com.example.i6contacts.model.ContactRecord;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CallsFragment extends Fragment {

	public ListView callsView;
	public CallsAdapter callsAdapter;
	RelativeLayout optionsBar = null;
	int isOptionsBarOpen = 0;

	public CallsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_calls_list,
				container, false);
		callsView = (ListView) rootView.findViewById(R.id.calls_list3);
		callsAdapter = new CallsAdapter(ContactsManager.CallsList);
		callsView.setAdapter(callsAdapter);

		callsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				CallRecord call = (CallRecord) callsAdapter.getItem(position);
				ContactsManager.setActiveCall(call);

				//if (optionsBar == null) {
					optionsBar = (RelativeLayout) getActivity().findViewById(
							R.id.callPanel3);
				//}
				((TextView) optionsBar.findViewById(R.id.call_name3))
						.setText(call.getName());
				showCallPanel();

				// TODO Auto-generated method stub

			}
		});
		callsView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				hideCallPanel();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		});
		return rootView;
	}

	void hideCallPanel() {
		//if (optionsBar == null) {
			optionsBar = (RelativeLayout) getActivity().findViewById(
					R.id.callPanel3);
		//}
		if (isOptionsBarOpen == 1) {
			try {
				ResizeAnimation anim = new ResizeAnimation(optionsBar);
				anim.setDuration(500);
				anim.setRightMarginParams(0, -320);
				optionsBar.startAnimation(anim);
				isOptionsBarOpen = 0;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	void showCallPanel() {
		Log.d("Call Panel", "Show");
		//if (optionsBar == null) {
			optionsBar = (RelativeLayout) getActivity().findViewById(
					R.id.callPanel3);
		//}
		ResizeAnimation anim = new ResizeAnimation(optionsBar);
		anim.setDuration(500);
		anim.setRightMarginParams(-320, 0);
		optionsBar.startAnimation(anim);
		isOptionsBarOpen = 1;
	}
}