package com.example.i6contacts.fragments;

import i6Contacts.Animation.ResizeAnimation;

import java.io.BufferedInputStream;
import java.io.InputStream;

import com.example.i6contacts.ContactsManager;
import com.example.i6contacts.R;
import com.example.i6contacts.adapters.ContactsAdapter;
import com.example.i6contacts.model.ContactRecord;
import com.example.i6contacts.utils.ImageHelper;
import com.example.i6contacts.utils.SoftKeyboard;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

public class ContactsFragment extends Fragment {

	public ListView contacts = null;
	public ContactsAdapter contactsAdapter = null;
	public EditText searchBar = null;
	public ImageButton btnSearch2 = null;
	View rootView = null;
	String contactNumber = "";
	RelativeLayout sidebar = null;
	int isSidebarOpen = 0;

	// viewMode: 0 for only contacts list visible
	// 1 for sidebar visible as well
	static int viewMode = 0;

	public ContactsFragment() {
	}

	static View prevView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contacts_list,
				container, false);
		contacts = (ListView) rootView.findViewById(R.id.contacts_list);
		searchBar = (EditText) rootView.findViewById(R.id.contacts_search2);
		btnSearch2 = (ImageButton) rootView.findViewById(R.id.btnSearch2);

		if (contactsAdapter == null) {
			contactsAdapter = new ContactsAdapter(ContactsManager.ContactsList);
		}
		contacts.setAdapter(contactsAdapter);
		contacts.setFastScrollEnabled(true);

		contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View itemView,
					int position, long arg3) {
				if (sidebar == null) {
					sidebar = (RelativeLayout) getActivity().findViewById(
							R.id.sidebar);
				}

				if (prevView != null)
					prevView = itemView;

				ContactRecord contact = (ContactRecord) contactsAdapter
						.getItem(position);
				String contactId = contact.getContactId();
				ContactsManager.setActiveContact(contact);
				// startActivity(intent);

				// searchBar.setVisibility(View.GONE);
				// adjust width
				// params.width = 300;

				//ResizeAnimation anim = new ResizeAnimation(sidebar);
				//anim.setDuration(500);
				// anim.setWidthParams(params.width, 300);
				// anim.setLeftMarginParams(-300,0);
				//anim.setRightMarginParams(-300, 0);
				//sidebar.startAnimation(anim);
				showSidebar(position);

				// blankAreaParams.width = 200;

				// Set Contact name
				TextView contactName = (TextView) sidebar
						.findViewById(R.id.contact_name);
				contactName.setText(contact.getName());

				// Set Contact name
				TextView contactNumber = (TextView) sidebar
						.findViewById(R.id.contact_number);
				contactNumber.setText(contact.getNumber());

				// Set image
				Uri picUri = getPhotoUri(contactId);
				ImageView profile = (ImageView) sidebar
						.findViewById(R.id.contact_image);

				if (picUri != null) {
					Uri my_contact_Uri = Uri.withAppendedPath(
							ContactsContract.Contacts.CONTENT_URI, contactId);
					InputStream photo_stream = ContactsContract.Contacts
							.openContactPhotoInputStream(getActivity()
									.getContentResolver(), my_contact_Uri);
					BufferedInputStream buf = new BufferedInputStream(
							photo_stream);
					Bitmap my_btmp = BitmapFactory.decodeStream(buf);

					// Round the corners
					my_btmp = ImageHelper.getRoundedCornerBitmap(my_btmp, 10);

					profile.setImageBitmap(my_btmp);
				} else {
					profile.setBackgroundResource(R.drawable.contact_default_image);
				}
				sidebar.setOnClickListener(null);
				sidebar.bringToFront();
			}
		});

		contacts.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
				
			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				hideSidebar();

			}
		});

		// Set search listener
		searchBar.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable searchText) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence searchText, int arg1,
					int arg2, int arg3) {
				Log.d("Search Text", searchText.toString());
				contactsAdapter.filter(searchText.toString());

			}
		});

		btnSearch2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (searchBar.getVisibility() == View.VISIBLE) {
					searchBar.setVisibility(View.GONE);
				} else {
					searchBar.setVisibility(View.VISIBLE);
				}
			}
		});

		searchBar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {

				} else {
					// SoftKeyboard.hideSoftKeyboard(getActivity());
				}
			}
		});

		return rootView;
	}

	public Uri getPhotoUri(String contactId) {
		ContentResolver contentResolver = getActivity().getContentResolver();

		try {
			Cursor cursor = contentResolver
					.query(ContactsContract.Data.CONTENT_URI,
							null,
							ContactsContract.Data.CONTACT_ID
									+ "="
									+ contactId
									+ " AND "

									+ ContactsContract.Data.MIMETYPE
									+ "='"
									+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
									+ "'", null, null);

			if (cursor != null) {
				if (!cursor.moveToFirst()) {
					return null; // no photo
				}
			} else {
				return null; // error in cursor process
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		Uri person = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));
		return Uri.withAppendedPath(person,
				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	}

	void hideSidebar() {
		
		if (sidebar == null) {
			sidebar = (RelativeLayout) getActivity().findViewById(
					R.id.sidebar);
		}
		if (isSidebarOpen == 1) {
			try {
				ResizeAnimation anim = new ResizeAnimation(sidebar);
				anim.setDuration(500);
				anim.setRightMarginParams(0, -300);
				sidebar.startAnimation(anim);
				isSidebarOpen = 0;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	void showSidebar(int position) {
		if (sidebar == null) {
			sidebar = (RelativeLayout) getActivity().findViewById(
					R.id.sidebar);
		}

		ContactRecord contact = (ContactRecord) contactsAdapter
				.getItem(position);
		String contactId = contact.getContactId();
		ContactsManager.setActiveContact(contact);

		ResizeAnimation anim = new ResizeAnimation(sidebar);
		anim.setDuration(500);
		anim.setRightMarginParams(-300, 0);
		sidebar.startAnimation(anim);
		isSidebarOpen = 1;
	}

}
